//
// Created by dawid on 29.06.16.
//

#include "SDL2_memory.h"

void SDL2_memory::SDL2_del::operator()(SDL_Surface *pSurface) {
  if (pSurface != nullptr) {
    SDL_FreeSurface(pSurface);
  }
}

