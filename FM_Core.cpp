//
// Created by dawid on 21.06.16.
//

#include <SDL2/SDL_video.h>
#include <SDL2/SDL.h>
#include <iostream>
#include "FM_Core.h"
#include "SDL2_memory.h"

SDL_Window* FM_Core::window;

FM_Core::~FM_Core() {
  this->window = nullptr;
}
bool FM_Core::Init(const InitDefines& initParam) {
  auto success = false;

  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    std::cerr << "Initialization error!\n" << SDL_GetError();
  } else {
    FM_Core::window = SDL_CreateWindow("Simple", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                                       initParam.width, initParam.height, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);

    if (FM_Core::window == nullptr) {
      std::cerr << "Create error!\n" << SDL_GetError();
    } else {
      success = true;
    }
  }

  return success;
}
void FM_Core::Close() {
  SDL_DestroyWindow(FM_Core::window);
  SDL_Quit();
}
void FM_Core::UpdateFrame() {
//  TODO move to another class
//  SDL_Rect stretch;
//  stretch.x = 0;
//  stretch.y = 0;
//  stretch.h = 320;
//  stretch.w = 320;
//  SDL_BlitScaled(this->surfacePicture, nullptr, this->mainWindowSurface, &stretch);
//  SDL_BlitSurface(this->surfacePicture, nullptr, this->mainWindowSurface, nullptr);
  SDL_UpdateWindowSurface(FM_Core::window);
}