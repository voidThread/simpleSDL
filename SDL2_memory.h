//
// Created by dawid on 29.06.16.
//
#pragma once

#include <memory>
#include <SDL2/SDL_surface.h>

namespace SDL2_memory {
  struct SDL2_del;

  using pSurface = std::unique_ptr<SDL_Surface, SDL2_del>;


  struct SDL2_del {
    void operator()(SDL_Surface* pSurface);
  };
};