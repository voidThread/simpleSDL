//
// Created by dawid on 21.06.16.
//
#pragma once

#include <SDL2/SDL.h>
#include <memory>
#include "SDL2_memory.h"

struct InitDefines {
  /*!
   *  \brief Initialization structure for main window SDL
   *
   */
  unsigned int height;  //!< Window height
  unsigned int width; //!< Window width
};

class FM_Core {
  /*!
   *  \brief Main class of SDL framework
   */
 protected:
  static SDL_Window *window;  //!< main static window pointer

 public:
  FM_Core() = default;  //!< set constructor to default
  FM_Core(const FM_Core&) = delete; //!< remove copy constructor
  virtual ~FM_Core();

  bool Init(const InitDefines &); //!< initialization main window with structure
  void UpdateFrame();
  void Close(); //!< kill sdl
};