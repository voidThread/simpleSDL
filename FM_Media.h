//
// Created by dawid on 28.06.16.
//
#pragma once

#include <SDL2/SDL_surface.h>
#include <vector>
#include <string>
#include <memory>

#include "FM_Core.h"

class FM_Media : FM_Core{
  /*!
   *  \brief class for media handle
   */
 private:
  std::vector< std::unique_ptr<SDL2_memory::pSurface> > surfacesVec;  //!< vector of loaded bitmaps

 public:
  FM_Media() = default;
  FM_Media(const FM_Media&);  //!< deep copy constructor
  virtual ~FM_Media() = default;

  bool LoadMedia(const std::string &mediaPath); //!< only load bitmaps
};