#include <iostream>
#include <SDL2/SDL.h>
#include "FM_Core.h"
#include "FM_Media.h"

using namespace std;

const auto SCREEN_WIDTH = 640;
const auto SCREEN_HEIGHT = 420;

int main() {
  FM_Core newSDL;
  FM_Media fm_media;

  InitDefines initParams;
  initParams.height = SCREEN_HEIGHT;
  initParams.width = SCREEN_WIDTH;

  newSDL.Init(initParams);

  fm_media.LoadMedia("assets/packed/hedgehog1.bmp");
  fm_media.LoadMedia("assets/packed/hedgehog2.bmp");

  auto quit = false;
  SDL_Event event;

  while (!quit) {
    while (SDL_PollEvent(&event) != 0) {
      if (event.type == SDL_QUIT)
        quit = true;
    }

    newSDL.UpdateFrame();
  }

  newSDL.Close();

  return 0;
}