//
// Created by dawid on 28.06.16.
//

#include <iostream>
#include "FM_Media.h"

FM_Media::FM_Media(const FM_Media& mediaParam) {
  surfacesVec.reserve(mediaParam.surfacesVec.size());

  for (auto&& it: mediaParam.surfacesVec) {
    surfacesVec.emplace_back(std::move(it.get()));
  }
}
bool FM_Media::LoadMedia(const std::string &mediaPath) {
  using namespace std;

  auto success = false;

  unique_ptr<SDL2_memory::pSurface> surfaceToSave (std::make_unique<SDL2_memory::pSurface >(SDL_LoadBMP(mediaPath.c_str())));

  if(surfaceToSave == nullptr) {
    cerr << "Load media error!\n" << SDL_GetError();
  } else {
    this->surfacesVec.push_back(std::move(surfaceToSave));
    success = true;
  }

  return success;
}